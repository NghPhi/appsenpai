package com.example.appsenpai.screen.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.appsenpai.R;
import com.example.appsenpai.screen.BaseActivity;
import com.example.appsenpai.screen.MainActivity;

public class SplashActivity extends BaseActivity implements ISplashContract.ISplashView {
    private SplashImp mSplashImp;

    @Override
    protected void initViews() {
        mSplashImp =new SplashImp(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mSplashImp.doMain();

            }
        },1000);
    }

    @Override
    protected void handleEvents() {

    }

    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void setPresenter(ISplashContract.ISplashPresenter presenter) {

    }
}