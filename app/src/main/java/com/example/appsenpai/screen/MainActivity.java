package com.example.appsenpai.screen;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.MenuItem;
import com.example.appsenpai.R;
import com.example.appsenpai.screen.main.fragmenthome.HomeFragment;
import com.example.appsenpai.screen.main.fragmentmore.MoreFragment;
import com.example.appsenpai.screen.main.fragmentpractice.PracticeFragment;
import com.example.appsenpai.screen.main.fragmentstudy.LessionFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends BaseActivity {
    private BottomNavigationView mBottomNav;
    private ViewPager viewPager;

    @Override
    protected void initViews() {
        initView();
        initPager();
    }

    @Override
    protected void handleEvents() {
        mBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        swipeToHome();
                        return true;
                    case R.id.nav_study:
                        swipeToTabStudy();
                        return true;
                    case R.id.nav_practice:
                        swipeToTabPractice();
                        return true;
                    case R.id.nav_more:
                        swipeToTabMore();
                        return true;
                }
                return false;
            }
        });
    }

    private void initView() {
        mBottomNav = findViewById(R.id.main_act_bottom_view);
        viewPager = findViewById(R.id.main_act_vp);
    }


    private void initPager() {
        viewPager.setOffscreenPageLimit(4);
        FragmentPagerAdapter adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return new HomeScreenFragment();
                    case 1:
                        return new LessionFragment();
                    case 2:
                        return new PracticeFragment();
                    case 3:
                        return new MoreFragment();
                }
                return new HomeScreenFragment();
            }

            @Override
            public int getCount() {
                return 4;
            }
        };
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        mBottomNav.setSelectedItemId(R.id.nav_home);
                        break;
                    case 1:
                        mBottomNav.setSelectedItemId(R.id.nav_study);
                        break;
                    case 2:
                        mBottomNav.setSelectedItemId(R.id.nav_practice);
                        break;
                    case 3:
                        mBottomNav.setSelectedItemId(R.id.nav_more);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }
    private void swipeToHome() {
//        viewPager.setCurrentItem(0);
    }

    private void swipeToTabStudy() {
//        viewPager.setCurrentItem(1);
    }

    private void swipeToTabPractice() {
//        viewPager.setCurrentItem(2);
    }

    private void swipeToTabMore() {
//        viewPager.setCurrentItem(3);
    }
}