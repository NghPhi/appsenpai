package com.example.appsenpai.screen;

import android.os.Bundle;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        initViews();
        handleEvents();
    }

    protected abstract void initViews();
    protected abstract void handleEvents();
    public abstract @LayoutRes
    int getLayout();
}
