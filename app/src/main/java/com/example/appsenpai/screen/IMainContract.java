package com.example.appsenpai.screen;

import com.example.appsenpai.network.IPresenter;
import com.example.appsenpai.common.IView;

public class IMainContract {
    interface IMainView extends IView<IMainPresenter> {

    }

    interface IMainPresenter extends IPresenter {

    }

}
