package com.example.appsenpai.screen.splash;

import android.content.Context;

import com.example.appsenpai.common.IPresenter;
import com.example.appsenpai.common.IView;


public interface ISplashContract {

    interface ISplashView extends IView<ISplashPresenter> {
        void goToMain();
    }

    interface ISplashPresenter extends IPresenter {
        void doMain();
    }

}
