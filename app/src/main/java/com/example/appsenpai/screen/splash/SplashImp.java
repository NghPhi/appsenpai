package com.example.appsenpai.screen.splash;

public class SplashImp implements ISplashContract.ISplashPresenter {

    private ISplashContract.ISplashView mSplashView;

    public SplashImp(ISplashContract.ISplashView splashView) {
        this.mSplashView = splashView;
        mSplashView.setPresenter(this);
    }

    @Override
    public void doMain() {
        mSplashView.goToMain();
    }

}
