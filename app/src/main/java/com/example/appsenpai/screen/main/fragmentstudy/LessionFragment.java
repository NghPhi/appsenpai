package com.example.appsenpai.screen.main.fragmentstudy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appsenpai.R;
import com.example.appsenpai.adapter.LessionAdapter;
import com.example.appsenpai.common.model.ILessionContract;
import com.example.appsenpai.common.model.LessionResponse;
import com.example.appsenpai.network.LessionImp;

import java.util.List;


public class LessionFragment extends Fragment implements ILessionContract.ILessionView {
    LessionImp mLessionImp;
    private LessionAdapter lessionAdapter;
    List<LessionResponse.Lession> mlist;
    private RecyclerView rvLession;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_lessionlearning, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLessionImp = new LessionImp(this);
        lessionAdapter = new LessionAdapter();
        rvLession = view.findViewById(R.id.rvLesson);
        rvLession.setAdapter(lessionAdapter);
        mLessionImp.getLession();

    }

    @Override
    public void getLessionSuccess(LessionResponse lessionResponse) {
        lessionAdapter.submitList(lessionResponse.getLession());
    }

    @Override
    public void getLessionFail(String msg) {
        Toast.makeText(getContext(), "Error: " + msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setPresenter(ILessionContract.ILessionPresenter presenter) {

    }
}
