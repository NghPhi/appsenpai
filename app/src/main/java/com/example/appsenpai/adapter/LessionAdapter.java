package com.example.appsenpai.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appsenpai.R;
import com.example.appsenpai.common.model.LessionResponse;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LessionAdapter extends RecyclerView.Adapter<LessionAdapter.LessionViewHolder> {
    private List<LessionResponse.Lession> mLessionList;

    public LessionAdapter() {
        mLessionList = new ArrayList<>();
    }

    public void submitList(List<LessionResponse.Lession> lessionList) {
        this.mLessionList.clear();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

        }
        this.mLessionList.addAll(lessionList);
        notifyDataSetChanged();
    }

    public LessionResponse.Lession getItem(int pos) {
        return mLessionList.get(pos);
    }

    @NonNull
    @Override
    public LessionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LessionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lession, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LessionViewHolder holder, int position) {
        holder.bindItem(mLessionList.get(position));
    }

    @Override
    public int getItemCount() {
        return mLessionList == null ? 0 : mLessionList.size();
    }

    public class LessionViewHolder extends RecyclerView.ViewHolder {

        public LessionViewHolder(@NonNull View itemView) {

            super(itemView);
        }

        public void bindItem(LessionResponse.Lession lession) {
            ((TextView) itemView.findViewById(R.id.text_menu)).setText(lession.getTitle());
            ((TextView) itemView.findViewById(R.id.text_tile)).setText(lession.getVietnamese());

        }
    }
}