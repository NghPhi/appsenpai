package com.example.appsenpai.network;


import com.example.appsenpai.common.model.LessionResponse;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkController {
        private static volatile IApi mCustomerApi;
        private static OkHttpClient okHttpClient = null;

        private static IApi getLessionApi() {
            okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .build();
            if (mCustomerApi == null) {
                Retrofit retrofit = new Retrofit.Builder()
                        .client(okHttpClient)
                        .baseUrl("http://demo5040912.mockable.io/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                mCustomerApi = retrofit.create(IApi.class);
            }
            return mCustomerApi;
        }

        public static void getLession(Callback<LessionResponse> callback) {
            getLessionApi().getLession().enqueue(callback);
        }

    }


