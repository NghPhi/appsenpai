package com.example.appsenpai.network;



import com.example.appsenpai.common.model.ILessionContract;
import com.example.appsenpai.common.model.LessionResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LessionImp implements ILessionContract.ILessionPresenter {
    private ILessionContract.ILessionView mLessionView;

    public LessionImp(ILessionContract.ILessionView lessionView) {
        this.mLessionView = lessionView;
        mLessionView.setPresenter((ILessionContract.ILessionPresenter) this);
    }

    @Override
    public void getLession() {
        NetworkController.getLession(new Callback<LessionResponse>() {
            @Override
            public void onResponse(Call<LessionResponse> call, Response<LessionResponse> response) {
                if (response.body() != null) {
                    mLessionView.getLessionSuccess(response.body());
                } else {
                    mLessionView.getLessionFail("Null");
                }
            }

            @Override
            public void onFailure(Call<LessionResponse> call, Throwable t) {
                mLessionView.getLessionFail(t.getMessage());
            }
        });
    }
}


