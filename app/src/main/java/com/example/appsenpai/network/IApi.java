package com.example.appsenpai.network;



import com.example.appsenpai.common.model.LessionResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IApi {
    @GET("lession")
    Call<LessionResponse> getLession();
}
