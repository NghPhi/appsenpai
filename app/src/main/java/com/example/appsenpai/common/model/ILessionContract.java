package com.example.appsenpai.common.model;

import com.example.appsenpai.common.IView;
import com.example.appsenpai.network.IPresenter;


public interface ILessionContract {
    interface ILessionView extends IView<ILessionPresenter> {
        void getLessionSuccess(LessionResponse lessionResponse);
        void getLessionFail(String msg);
    }

    interface ILessionPresenter extends IPresenter {
        void getLession();
    }
}
