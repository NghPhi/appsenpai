package com.example.appsenpai.common;

public interface IView<T> {
    void setPresenter(T presenter);
}
