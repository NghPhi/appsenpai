
package com.example.appsenpai.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LessionResponse {

    @SerializedName("lession")
    @Expose
    private List<Lession> lession = null;

    public List<Lession> getLession() {
        return lession;
    }

    public void setLession(List<Lession> lession) {
        this.lession = lession;
    }

    public class Lession {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("vietnamese")
        @Expose
        private String vietnamese;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getVietnamese() {
            return vietnamese;
        }

        public void setVietnamese(String vietnamese) {
            this.vietnamese = vietnamese;
        }

    }
}